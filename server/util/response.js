

exports.responseData = (data) => {
    return {
        data
    }

};
exports.responseError = (error_code, error_message, error_data) => {
    return {
        error_code,
        error_message,
        error_data
    }

};
