const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const urlSchema = new Schema({
    url: {
        type: String,
        required: true,
        index: true
    },
    tinyurl: {
        type: String,
        required: true,
        index: true
    }


});


module.exports = mongoose.model('Url', urlSchema);
