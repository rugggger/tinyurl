import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { DetailsComponent } from './components/details/details.component';
import {TinyUrlFormComponent} from './components/tiny-url-form/tiny-url-form.component';
import {UrlService} from './url.service';

@NgModule({
  declarations: [
    AppComponent,
    DetailsComponent,
    TinyUrlFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    UrlService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
