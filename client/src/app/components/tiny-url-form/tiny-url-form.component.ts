import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UrlService} from '../../url.service';

@Component({
  selector: 'app-tiny-url-form',
  templateUrl: './tiny-url-form.component.html',
  styleUrls: ['./tiny-url-form.component.scss']
})
export class TinyUrlFormComponent implements OnInit {

  tinyurlForm: FormGroup;
  tinyurlLink = '';

  constructor(
    private fb: FormBuilder,
    private urlService: UrlService
  ) {
    this.tinyurlForm = this.fb.group({
      url: ['' ],
      tinyurl: [''],
    });
  }

  ngOnInit() {
  }

  onInput() {
    this.tinyurlForm.get('tinyurl').setValue('');

  }
  submitForm() {
    const values = this.tinyurlForm.value;
    // send form values (url/tinyurl and get the matching Url document if exists,
    // if the url doesn't exists - create it and return the tiny url
    this.urlService.getUrl(values)
      .subscribe(urlDoc => {
        this.tinyurlForm.get('url').setValue(urlDoc.url);
        this.tinyurlForm.get('tinyurl').setValue(urlDoc.tinyurl);
        this.tinyurlLink = `http://localhost:3000/${urlDoc.tinyurl}`;
      });
  }

}
