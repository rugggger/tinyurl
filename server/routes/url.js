const express = require('express');
const router = express.Router();
const urlController = require('../controllers/url');

router.post('/api/url', urlController.saveTinyUrl);
router.get('/:tinyurl', urlController.redirectByTinyUrl);


module.exports = router;
