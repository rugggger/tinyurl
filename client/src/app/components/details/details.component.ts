import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  authForm: FormGroup;

  constructor(
    private fb: FormBuilder
  ) {
    this.authForm = this.fb.group({
      username: ['', Validators.required ],
      email: ['', [Validators.required, Validators.email] ],
      password: ['', Validators.required ]
    });
  }

  ngOnInit() {
  }

  submitForm() {
    console.log('form ', this.authForm);
    const credentials = this.authForm.value;
    console.log(credentials);
  }

}
