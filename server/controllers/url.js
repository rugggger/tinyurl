const Url = require('../models/url');
const {responseData, responseError} = require('../util/response');

const LENGTH=8;



function createTinyUrl(){
    let result           = '';
    const characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for ( let i = 0; i < LENGTH; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

exports.saveTinyUrl = async (req, res, next) => {
    const url = req.body.url;
    let urlExists = await Url.find({url});
    if (urlExists.length) {
        // return url document
        res.json(responseData(urlExists[0]));
        return;
    }
    // else create a new tinyUrl
    let tinyurl;
    do {
        tinyurl = createTinyUrl();
        urlExists = await Url.find({tinyurl});
    }
    // repeat until created a tinyUrl that doesn't exist already
    while (urlExists.length===1);

    const urlDocument = new Url({url: url, tinyurl});
    urlDocument.save()
        .then(newUrl => res.json(responseData(newUrl)));
};

exports.redirectByTinyUrl = async (req, res, next) => {
    const tinyurl = req.params.tinyurl;
    // search the tinyurl in the database
    const UrlRecords = await Url.find({tinyurl});

    // if found , redirect to it
    if (UrlRecords.length>0) {
        const redirect = UrlRecords[0].url;
        res.redirect(redirect);
    } else {
        res.send('No URL found');
    }

};
