import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {IUrl} from './models/models';

interface IError {
  error_code: string;
  error_message: string;
  error_data?: object;
}
interface IResponse<T> {
  data?: T;
}

interface IErrorResponse<T> {
  error: IError;
}
@Injectable({
  providedIn: 'root'
})
export class UrlService {
  readonly baseurl = 'http://localhost:3000/api';

  constructor(private http: HttpClient) { }
  getUrl(values): Observable<IUrl> {
    const apiurl = `${this.baseurl}/url`;
    return this.http.post(apiurl, values)
      .pipe(
        map((res: IResponse<IUrl>) => {
          return res.data;
        }),
        catchError((error: IErrorResponse<IError>) => {
          console.log('error I got was ', error.error.error_code);
          console.log('error I got was ', error.error.error_message);
          return throwError(error.error);
        }));
  }
}
